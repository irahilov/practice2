import java.util.ArrayList;
import java.util.List;

interface IWeatherServiceFactory {
    IWeatherService createWeatherService();
}

// Конкретная реализация фабрики для создания объектов WeatherServiceImpl от Яндекса
class YandexWeatherService implements IWeatherServiceFactory {
    @Override
    public IWeatherService createWeatherService() {
        return new YandexWeather();
    }
}

// Конкретная реализация фабрики для создания объектов WeatherServiceImpl от Google
class GoogleWeatherService implements IWeatherServiceFactory {
    @Override
    public IWeatherService createWeatherService() {
        return new GoogleWeather();
    }
}

// Интерфейс для погодного сервиса
interface IWeatherService {
    void addObserver(WeatherObserver observer);
    void removeObserver(WeatherObserver observer);
    void notifyObservers();
    void fetchData();
    String getSource();
}

// Реализация погодного сервиса от Яндекса
class YandexWeather implements IWeatherService {
    private List<WeatherObserver> observers = new ArrayList<>();
    private Data weatherData;

    @Override
    public void addObserver(WeatherObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(WeatherObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (WeatherObserver observer : observers) {
            observer.update(weatherData);
        }
    }

    @Override
    public void fetchData() {
        // Реализация для получения данных о погоде от Яндекса
        // В данном примере просто создаем фиктивные данные
        weatherData = new Data(28.0, "Облачно", "Яндекс");
        notifyObservers();
    }

    @Override
    public String getSource() {
        return "Яндекс";
    }
}

// Реализация погодного сервиса от Google
class GoogleWeather implements IWeatherService {
    private List<WeatherObserver> observers = new ArrayList<>();
    private Data weatherData;

    @Override
    public void addObserver(WeatherObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(WeatherObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (WeatherObserver observer : observers) {
            observer.update(weatherData);
        }
    }

    @Override
    public void fetchData() {
        // Реализация для получения данных о погоде от Google
        // В данном примере просто создаем фиктивные данные
        weatherData = new Data(23.5, "Дождь", "Google");
        notifyObservers();
    }

    @Override
    public String getSource() {
        return "Google";
    }
}

// Интерфейс для наблюдателя погоды
interface IWeatherObserver {
    void update(Data weatherData);
}

// Реализация наблюдателя погоды
class WeatherObserver implements IWeatherObserver {
    @Override
    public void update(Data weatherData) {
        System.out.println("Текущая погода от " + weatherData.getSource() + ": " + weatherData.getTemperature() + "°C, " + weatherData.getCondition());
    }
}

// Модель данных о погоде
class Data {
    private double temperature;
    private String condition;
    private String source;

    public Data(double temperature, String condition, String source) {
        this.temperature = temperature;
        this.condition = condition;
        this.source = source;
    }

    public double getTemperature() {
        return temperature;
    }

    public String getCondition() {
        return condition;
    }

    public String getSource() {
        return source;
    }
}

// Адаптер для связи между погодным сервисом и наблюдателем
class Adapter {
    private IWeatherService weatherService;
    private WeatherObserver weatherObserver;

    public Adapter(IWeatherServiceFactory factory) {
        weatherService = factory.createWeatherService();
        weatherObserver = new WeatherObserver();
        weatherService.addObserver(weatherObserver);
    }

    public void fetchWeather() {
        weatherService.fetchData();
    }
}

// Пример использования
public class Main {
    public static void main(String[] args) {
        IWeatherServiceFactory factory1 = new YandexWeatherService();
        IWeatherServiceFactory factory2 = new GoogleWeatherService();
        
        Adapter adapterYandex = new Adapter(factory1);
        Adapter adapterGoogle = new Adapter(factory2);
        
        adapterYandex.fetchWeather();
        adapterGoogle.fetchWeather(); 
    }
}
