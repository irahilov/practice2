```plantuml
@startuml

abstract class WeatherProvider {
    {abstract} +createWeatherData(): WeatherData
    +fetchData()
}

class APIWeatherProvider {
    +createWeatherData(): WeatherData
}

class DatabaseWeatherProvider {
    +createWeatherData(): WeatherData
}

class WeatherProviderFactory {
    +createWeatherProvider(providerType: String): WeatherProvider
}

class WeatherData {
    -temperature: String
    -humidity: String
    -condition: String
    +WeatherData(temperature: String, humidity: String, condition: String)
    +getTemperature(): String
    +getHumidity(): String
    +getCondition(): String
}

abstract class WeatherObserver {
    {abstract} +update(weatherData: WeatherData)
}

class ConcreteWeatherObserver1 {
    +update(weatherData: WeatherData)
}

class ConcreteWeatherObserver2 {
    +update(weatherData: WeatherData)
}

class WeatherSubject {
    -observers: List<WeatherObserver>
    +addObserver(observer: WeatherObserver)
    +removeObserver(observer: WeatherObserver)
    +notifyObservers(weatherData: WeatherData)
}

WeatherProvider <|-- APIWeatherProvider
WeatherProvider <|-- DatabaseWeatherProvider
WeatherProvider <|-- ThirdPartyWeatherServiceAdapter
WeatherObserver <|-- ConcreteWeatherObserver1
WeatherObserver <|-- ConcreteWeatherObserver2

WeatherProvider ..> WeatherData : creates
WeatherObserver ..> WeatherData : updates
WeatherSubject o--> WeatherObserver : notifies

@enduml
```
