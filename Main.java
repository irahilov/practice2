import java.util.ArrayList; 
import java.util.List; 
 
// Абстрактный класс WeatherProvider 
abstract class WeatherProvider { 
 public abstract WeatherData createWeatherData(); 
 
 public void fetchData() { 
 // Логика получения данных о погоде 
 WeatherData weatherData = createWeatherData(); 
 notifyObservers(weatherData); 
 } 
 
 // Абстрактный класс WeatherData 
 abstract static class WeatherData { 
 public abstract String getTemperature(); 
 public abstract String getHumidity(); 
 public abstract String getCondition(); 
 } 
 
 // Классы APIWeatherData и DatabaseWeatherData 
 static class APIWeatherData extends WeatherData { 
 private String temperature = "25°C"; 
 private String humidity = "70%"; 
 private String condition = "Sunny"; 
 
 @Override
 public String getTemperature() { return temperature; } 
 @Override
 public String getHumidity() { return humidity; } 
 @Override
 public String getCondition() { return condition; } 
 } 
 
 static class DatabaseWeatherData extends WeatherData { 
 private String temperature = "22°C"; 
 private String humidity = "60%"; 
 private String condition = "Cloudy"; 
 
 @Override
 public String getTemperature() { return temperature; } 
 @Override
 public String getHumidity() { return humidity; } 
 @Override
 public String getCondition() { return condition; } 
 } 
 
 // Фабричный метод для создания конкретного WeatherProvider 
 public static WeatherProvider createWeatherProvider(String providerType) { 
 if (providerType.equals("API")) { 
 return new APIWeatherProvider(); 
 } else if (providerType.equals("Database")) { 
 return new DatabaseWeatherProvider(); 
 } 
 return null; 
 } 
} 
 
// Конкретные реализации WeatherProvider 
class APIWeatherProvider extends WeatherProvider { 
 public WeatherData createWeatherData() { 
 // Логика получения данных о погоде с API 
 return new APIWeatherData(); 
 } 
} 
 
class DatabaseWeatherProvider extends WeatherProvider { 
 public WeatherData createWeatherData() { 
 // Логика получения данных о погоде из базы данных 
 return new DatabaseWeatherData(); 
 } 
} 
 
// Интерфейс WeatherObserver 
interface WeatherObserver { 
 void update(WeatherProvider.WeatherData weatherData); 
} 
 
// Конкретные реализации WeatherObserver 
class ConcreteWeatherObserver1 implements WeatherObserver { 
 public void update(WeatherProvider.WeatherData weatherData) { 
 // Логика обработки обновления данных о погоде для observer1 
 System.out.println("Observer 1: Temperature - " + weatherData.getTemperature() + 
 ", Humidity - " + weatherData.getHumidity() + 
 ", Condition - " + weatherData.getCondition()); 
 } 
} 
 
class ConcreteWeatherObserver2 implements WeatherObserver { 
 public void update(WeatherProvider.WeatherData weatherData) { 
 // Логика обработки обновления данных о погоде для observer2 
 System.out.println("Observer 2: Temperature - " + weatherData.getTemperature() + 
 ", Humidity - " + weatherData.getHumidity() + 
 ", Condition - " + weatherData.getCondition()); 
 } 
} 
 
// Класс WeatherSubject 
class WeatherSubject { 
 private List<WeatherObserver> observers = new ArrayList<>(); 
 
 public void addObserver(WeatherObserver observer) { 
 observers.add(observer); 
 } 
 
 public void removeObserver(WeatherObserver observer) { 
 observers.remove(observer); 
 } 
 
 public void notifyObservers(WeatherProvider.WeatherData weatherData) { 
 for (WeatherObserver observer : observers) { 
 observer.update(weatherData); 
 } 
 } 
} 
 
public class Main { 
 public static void main(String[] args) { 
 WeatherSubject subject = new WeatherSubject(); 
 WeatherObserver observer1 = new ConcreteWeatherObserver1(); 
 WeatherObserver observer2 = new ConcreteWeatherObserver2(); 
 
 subject.addObserver(observer1); 
 subject.addObserver(observer2); 
 
 WeatherProvider weatherProvider = WeatherProvider.createWeatherProvider("API"); // Можно заменить на "Database" 
 
 subject.notifyObservers(weatherProvider.fetchData()); 
 } 
}
