```plantuml
@startuml

interface IWeatherServiceFactory {
    +createWeatherService(): WeatherService
}

class YandexWeatherService implements IWeatherServiceFactory {
    +createWeatherService(): WeatherService
}

class GoogleWeatherService implements IWeatherServiceFactory {
    +createWeatherService(): WeatherService
}

interface IWeatherService {
    +addObserver(observer: WeatherObserver): void
    +removeObserver(observer: WeatherObserver): void
    +notifyObservers(): void
    +fetchData(): void
    +getSource(): String
}

class YandexWeather implements IWeatherService {
    -observers: List<WeatherObserver>
    -weatherData: Data
    +addObserver(observer: WeatherObserver): void
    +removeObserver(observer: WeatherObserver): void
    +notifyObservers(): void
    +fetchData(): void
    +getSource(): String
}

class GoogleWeather implements IWeatherService {
    -observers: List<WeatherObserver>
    -weatherData: Data
    +addObserver(observer: WeatherObserver): void
    +removeObserver(observer: WeatherObserver): void
    +notifyObservers(): void
    +fetchData(): void
    +getSource(): String
}

interface IWeatherObserver {
    +update(weatherData: Data): void
}

class WeatherObserver implements IWeatherObserver {
    +update(weatherData: Data): void
}

class Data {
    -temperature: double
    -condition: String
    -source: String
    +Data(temperature: double, condition: String, source: String)
    +getTemperature(): double
    +getCondition(): String
    +getSource(): String
}

class Adapter {
    -weatherService: WeatherService
    -weatherObserver: WeatherObserver
    +Adapter(factory: IWeatherServiceFactory)
    +fetchWeather(): void
}

class Main {
    +main(args: String[]): void
}

IWeatherService "1" --o "*" WeatherObserver
IWeatherService o-- Data

Adapter "1" --o "1" IWeatherServiceFactory
Adapter o-- IWeatherService
Adapter o-- WeatherObserver

Main --> Adapter

@enduml
```
